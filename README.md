Application to calculate running pace in unit mm:ss min/km

We can get running paces for popular time limits in distances: 10km, half-marathon and marathon.
Possibility to calculate pace for custom parameters distance and time

API:
GET:
- /ten
- /half
- /marathon

POST:
- /calculate, with RequestBody, example: {
                                         "distance": 5,
                                         "time": "00:18:00"
                                         }