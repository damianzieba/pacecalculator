package com.example.dzcompany.pacecalculator.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PaceTimeException extends RuntimeException {
	public static final String MESSAGE = "Wrong time format, use - hh::mm:ss";
	public static final String TIME_IS_EMPTY = "Time is empty";

	public PaceTimeException() {
		super(MESSAGE);
	}

	public PaceTimeException(String message) {
		super(message);
	}
}
