package com.example.dzcompany.pacecalculator.controller;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.dzcompany.pacecalculator.model.RunParam;
import com.example.dzcompany.pacecalculator.model.RunningPace;
import com.example.dzcompany.pacecalculator.service.PaceCalculatorService;

@RestController
public class PaceCalculatorController {
	private PaceCalculatorService paceCalculatorService;

	public PaceCalculatorController(PaceCalculatorService paceCalculatorService) {
		this.paceCalculatorService = paceCalculatorService;
	}

	@GetMapping("/ten")
	public List<RunningPace> getPeacesForTen() {
		return paceCalculatorService.getPeacesForTen();
	}

	@GetMapping("/half")
	public List<RunningPace> getPeacesForHalfMarathon() {
		return paceCalculatorService.getPeacesForHalfMarathon();
	}

	@GetMapping("/marathon")
	public List<RunningPace> getPeacesForMarathon() {
		return paceCalculatorService.getPeacesForMarathon();
	}


	@PostMapping("/calculate")
	public RunningPace calculatePeace(@RequestBody RunParam runParam) {
		return paceCalculatorService.getPeace(runParam);
	}
}
