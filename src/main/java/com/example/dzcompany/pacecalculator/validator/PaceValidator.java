package com.example.dzcompany.pacecalculator.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.springframework.stereotype.Component;
import com.example.dzcompany.pacecalculator.exceptions.PaceDistanceException;
import com.example.dzcompany.pacecalculator.exceptions.PaceTimeException;
import static com.example.dzcompany.pacecalculator.exceptions.PaceTimeException.TIME_IS_EMPTY;

@Component
public class PaceValidator {

	public static final String TIME_FORMAT = "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$";
	public static final String HH_MM_SS = "HH:mm:ss";

	public void validateDinstance(Double distance) {
		if (distance == null || distance <= 0.0) {
			throw new PaceDistanceException();
		}
	}

	public Calendar convertTime(String timeLimit) {
		validateTime(timeLimit);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(HH_MM_SS);
		try {
			cal.setTime(sdf.parse(timeLimit));
		} catch (ParseException e) {
			throw new PaceTimeException();
		}
		return cal;
	}

	public void validateTime(String timeLimit) {
		if (timeLimit == null || timeLimit.isEmpty()) {
			throw new PaceTimeException(TIME_IS_EMPTY);
		}
		if ( ! timeLimit.matches(TIME_FORMAT)) {
			throw new PaceTimeException();
		}
	}
}
