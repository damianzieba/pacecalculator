package com.example.dzcompany.pacecalculator.service;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.example.dzcompany.pacecalculator.model.RunParam;
import com.example.dzcompany.pacecalculator.model.RunningPace;
import com.example.dzcompany.pacecalculator.model.distance.Distance;
import com.example.dzcompany.pacecalculator.model.distance.HalfMarathon;
import com.example.dzcompany.pacecalculator.model.distance.Marathon;
import com.example.dzcompany.pacecalculator.model.distance.TenKm;
import com.example.dzcompany.pacecalculator.validator.PaceValidator;

@Service
public class PaceCalculatorService {

	public static final String PACE_UNIT = " min/km";
	public static final int NUMBER_OF_MINUTES = 60;
	public static final int NUMBER_OF_SECONDS = 60;

	private PaceValidator validator;

	public PaceCalculatorService(PaceValidator validator) {
		this.validator = validator;
	}

	public List<RunningPace> getPeacesForTen() {
		return getRunningPacesFor(new TenKm());
	}

	public List<RunningPace> getPeacesForHalfMarathon() {
		return getRunningPacesFor(new HalfMarathon());
	}

	public List<RunningPace> getPeacesForMarathon() {
		return getRunningPacesFor(new Marathon());
	}

	private List<RunningPace> getRunningPacesFor(Distance distance) {
		return distance.getTimeLimits().stream()
				.map(time -> buildPace(distance.getDistance(), time))
				.collect(Collectors.toList());
	}

	private RunningPace buildPace(Double distance, String timeLimit) {
		validator.validateDinstance(distance);
		Calendar time = validator.convertTime(timeLimit);

		RunningPace pace = RunningPace.builder()
				.distance(distance)
				.time(timeLimit)
				.paceValue(calculatePace(distance, time))
				.build();
		return pace;
	}

	private String calculatePace(Double distance, Calendar time) {
		double seconds = (time.get(Calendar.HOUR) * NUMBER_OF_MINUTES +  time.get(Calendar.MINUTE)) * NUMBER_OF_SECONDS +
				time.get(Calendar.SECOND);
		double pace = seconds / distance;
		int min = (int) (pace / NUMBER_OF_SECONDS);
		double sec = pace - min * NUMBER_OF_SECONDS;
		return formatPaceResult(min, sec);
	}

	private String formatPaceResult(int min, double sec) {
		DecimalFormat decimalFormat = new DecimalFormat("00");
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(decimalFormat.format(min));
		stringBuilder.append(":");
		stringBuilder.append(decimalFormat.format(sec));
		stringBuilder.append(PACE_UNIT);
		return stringBuilder.toString();
	}

	public RunningPace getPeace(RunParam runParam) {
		return buildPace(runParam.getDistance(), runParam.getTime());
	}
}
