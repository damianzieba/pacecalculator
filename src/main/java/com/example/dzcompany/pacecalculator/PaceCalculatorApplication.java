package com.example.dzcompany.pacecalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaceCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaceCalculatorApplication.class, args);
	}

}
