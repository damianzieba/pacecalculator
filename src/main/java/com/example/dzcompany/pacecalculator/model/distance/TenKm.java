package com.example.dzcompany.pacecalculator.model.distance;

public class TenKm implements Distance{

	@Override
	public Double getDistance() {
		return new Double(10.0);
	}
}
