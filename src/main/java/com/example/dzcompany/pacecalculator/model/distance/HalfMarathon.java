package com.example.dzcompany.pacecalculator.model.distance;

import java.util.ArrayList;
import java.util.List;

public class HalfMarathon implements Distance {
	@Override
	public Double getDistance() {
		return new Double(21.1);
	}

	@Override
	public List<String> getTimeLimits() {
		List<String> timeLimits = new ArrayList<>();
		timeLimits.add("02:00:00");
		timeLimits.add("01:40:00");
		timeLimits.add("01:30:00");
		timeLimits.add("01:25:00");
		return timeLimits;
	}
}
