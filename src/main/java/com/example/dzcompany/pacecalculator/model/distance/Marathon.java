package com.example.dzcompany.pacecalculator.model.distance;

import java.util.ArrayList;
import java.util.List;

public class Marathon implements Distance {
	@Override
	public Double getDistance() {
		return new Double(42.2);
	}

	@Override
	public List<String> getTimeLimits() {
		List<String> timeLimits = new ArrayList<>();
		timeLimits.add("04:00:00");
		timeLimits.add("03:30:00");
		timeLimits.add("03:00:00");
		timeLimits.add("02:30:00");
		return timeLimits;
	}
}
