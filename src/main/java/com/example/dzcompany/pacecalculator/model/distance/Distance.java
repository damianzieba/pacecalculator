package com.example.dzcompany.pacecalculator.model.distance;

import java.util.ArrayList;
import java.util.List;

public interface Distance {
	Double getDistance();
	default List<String> getTimeLimits() {
		List<String> timeLimits = new ArrayList<>();
		timeLimits.add("01:00:00");
		timeLimits.add("00:50:00");
		timeLimits.add("00:40:00");
		timeLimits.add("00:38:00");
		timeLimits.add("00:30:00");
		return timeLimits;
	}
}
