package com.example.dzcompany.pacecalculator.model;

import lombok.Builder;
import lombok.Getter;

@Getter
public class RunningPace extends RunParam {
	private String paceValue;

	@Builder
	public RunningPace(Double distance, String time, String paceValue) {
		super(distance, time);
		this.paceValue = paceValue;
	}
}
