package com.example.dzcompany.pacecalculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RunParam {
	private Double distance;
	private String time;
}
