package com.example.dzcompany.pacecalculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.example.dzcompany.pacecalculator.exceptions.PaceDistanceException;
import com.example.dzcompany.pacecalculator.model.RunParam;
import com.example.dzcompany.pacecalculator.model.RunningPace;
import com.example.dzcompany.pacecalculator.service.PaceCalculatorService;
import com.example.dzcompany.pacecalculator.validator.PaceValidator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaceCalculatorTest {

	private PaceValidator validator;
	private PaceCalculatorService paceCalculatorService;

	@BeforeEach
	void beforeEach() {
		validator = new PaceValidator();
		paceCalculatorService = new PaceCalculatorService(validator);
	}
	@Test
	void shouldReturnCorrectRunningPace() {
		// given
		String time = "0:18:00";
		double distance = 5.0;
		RunParam runParam = new RunParam(distance, time);
		// when

		RunningPace pace = paceCalculatorService.getPeace(runParam);
		// then
		assertEquals(time, pace.getTime());
		assertEquals(distance, pace.getDistance());
		assertEquals("03:36 min/km", pace.getPaceValue());
	}

	@Test
	void shouldTrowBadRequest() {
		// given
		String time = "0:40:00";
		double distance = 0;
		RunParam runParam = new RunParam(distance, time);
		// when
		Exception exception = assertThrows(PaceDistanceException.class, () -> {
			paceCalculatorService.getPeace(runParam);
		});
		// then
		String expectedMessage = PaceDistanceException.MESSAGE;
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

}
